{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}

module Web.Instapaper (
  getCredential,
  call,
  callWithResponse,
  getBookmarkHighlights,
  deleteHighlight,
  listBookmarks,
  listBookmarksDefault,
  listFolders,
  -- Re-exports from Web.Instapaper.Types
  BookmarkId(..),
  FolderId(..),
  HighlightId(..),
  UserId(..),
  Bookmark(..),
  Folder(..),
  Highlight(..),
  User(..),
  ListBookmarksResult(..),
  APIRequest, -- hide constructors
  InstapaperError(..),
) where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.Catch
import Control.Monad.Trans
import Control.Monad.Trans.Control
import Control.Monad.Trans.Resource
import qualified Data.ByteString.Char8 as C
import Data.Aeson
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Scientific
import Data.Text
import Data.Typeable
import Network.HTTP.Conduit
import Web.Authenticate.OAuth
import Web.Instapaper.Api
import Web.Instapaper.Types
-- https://www.instapaper.com/api

endpoint :: String
endpoint = "https://www.instapaper.com/api/1.1"

getCredential :: (MonadIO m, MonadThrow m, MonadBaseControl IO m) =>
    OAuth -> ByteString -> ByteString -> m Credential
getCredential oauth username password = do
  initReq <- parseUrl $ endpoint ++ "/oauth/access_token"
  let req = setQueryString [("x_auth_mode", Just "client_auth"), ("x_auth_username", Just username), ("x_auth_password", Just password)] $ initReq {method = "POST"}
  res <- withManager $ \m -> do
    tmp <- signOAuth oauth emptyCredential req
    httpLbs tmp m
  params <- mapM (\param -> case C.split '=' param of
    [key, val] -> return (key, val)
    otherwise -> throwM (PatternMatchFail $ show param)) $ C.split '&' $ toStrict $ responseBody res
  return $ Credential params

getReq :: MonadResource m => APIRequest apiName responseType -> m Request
getReq (APIRequestGet url params) = do
  req <- parseUrl $ endpoint ++ url
  return $ setQueryString params $ req {method = "GET"}
getReq (APIRequestPost url params) = do
  req <- parseUrl $ endpoint ++ url
  return $ setQueryString params $ req {method = "POST"}

call :: (MonadResource m, FromJSON responseType)
  => OAuth -> Credential -> Manager -> APIRequest apiName responseType -> m responseType
call oauth cred m apiReq = responseBody <$> callWithResponse oauth cred m apiReq

callWithResponse :: (MonadResource m, FromJSON responseType)
  => OAuth -> Credential -> Manager -> APIRequest apiName responseType -> m (Response responseType)
callWithResponse oauth cred m apiReq = do
  signedReq <- getReq apiReq >>= signOAuth oauth cred
  res <- httpLbs signedReq m
  case eitherDecodeStrict $ toStrict $ responseBody res of
    Right x -> return $ res {responseBody = x}
    Left err -> monadThrow $ FromJSONError err
