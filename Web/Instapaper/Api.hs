{-# LANGUAGE OverloadedStrings #-}

module Web.Instapaper.Api where

import Control.Applicative
import Data.ByteString.Char8 hiding (map)
import Data.Maybe
import Web.Instapaper.Types

data ListBookmarksOptions = ListBookmarksOptions {
  lbLimit :: Maybe Int,
  lbFolderId :: Maybe FolderId,
  lbBookmarksHave :: Maybe [BookmarkId], -- TODO: optionally provide hash values with bookmark IDs
  lbHighlightsHave :: Maybe [HighlightId]
}
listBookmarksOptionsDefault = ListBookmarksOptions Nothing Nothing Nothing Nothing

data ListBookmarks
listBookmarks :: ListBookmarksOptions -> APIRequest ListBookmarks ListBookmarksResult
listBookmarks opts = APIRequestGet {
  _url = "/bookmarks/list",
  _params = catMaybes [
    pairStringWithKey "limit" . pack . show <$> lbLimit opts,
    pairStringWithKey "folder_id" . pack . serialiseFolderId <$> lbFolderId opts,
    pairStringWithKey "have" . intercalate "," .
      map (pack . show . getBookmarkId) <$> lbBookmarksHave opts,
    pairStringWithKey "highlights" . intercalate "," .
      map (pack . show . getHighlightId) <$> lbHighlightsHave opts
  ]
  }
  where
  pairStringWithKey k v = (k, Just $ v)
  serialiseFolderId (FolderId f_id) = show f_id
  serialiseFolderId FolderUnread = "unread"
  serialiseFolderId FolderStarred = "starred"
  serialiseFolderId FolderArchive = "archive"

listBookmarksDefault :: APIRequest ListBookmarks ListBookmarksResult
listBookmarksDefault = listBookmarks listBookmarksOptionsDefault

data ListFolders
listFolders :: APIRequest ListFolders [Folder]
listFolders = APIRequestGet {
  _url = "/folders/list",
  _params = []
  }

data GetBookmarkHighlights
getBookmarkHighlights :: BookmarkId -> APIRequest GetBookmarkHighlights [Highlight]
getBookmarkHighlights bookmark = APIRequestGet {
  _url = "/bookmarks/" ++ (show $ getBookmarkId bookmark) ++ "/highlights",
  _params = []
  }

data DeleteHighlight
deleteHighlight :: HighlightId -> APIRequest DeleteHighlight ()
deleteHighlight highlight = APIRequestPost {
  _url = "/highlights/" ++ (show $ getHighlightId highlight) ++ "/delete",
  _params = []
  }
