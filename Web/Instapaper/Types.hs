{-# LANGUAGE DeriveDataTypeable, OverloadedStrings #-}
module Web.Instapaper.Types (
  BookmarkId(..),
  FolderId(..),
  HighlightId(..),
  UserId(..),
  Bookmark(..),
  Folder(..),
  Highlight(..),
  User(..),
  ListBookmarksResult(..),
  APIRequest(..),
  InstapaperError(..),
  -- Internal:
  APIParams,
) where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Scientific
import Data.Text
import Data.Typeable

newtype BookmarkId = BookmarkId {getBookmarkId :: Int} deriving (Eq, Ord, Show)
instance FromJSON BookmarkId where parseJSON v = BookmarkId <$> parseJSON v

data FolderId =
  FolderId {getFolderId :: Int}
  | FolderUnread
  | FolderStarred
  | FolderArchive
  deriving Show
instance FromJSON FolderId where parseJSON v = FolderId <$> parseJSON v

newtype HighlightId = HighlightId {getHighlightId :: Int} deriving (Eq, Ord, Show)
instance FromJSON HighlightId where parseJSON v = HighlightId <$> parseJSON v

newtype UserId = UserId {getUserId :: Int} deriving (Eq, Ord, Show)
instance FromJSON UserId where parseJSON v = UserId <$> parseJSON v

data Bookmark = Bookmark {
  bBookmarkId :: BookmarkId,
  bUrl :: Text,
  bTitle :: Text,
  bDescription :: Text
  }
  deriving Show
instance FromJSON Bookmark where
  parseJSON (Object v) = Bookmark <$>
                          v .: "bookmark_id" <*>
                          v .: "url" <*>
                          v .: "title" <*>
                          v .: "description"
  parseJSON _   = mzero
-- TODO: add assert that "type" == "bookmark"

data Folder = Folder {
  fFolderId :: FolderId,
  fTitle :: Text,
  fDisplayTitle :: Text,
  fSyncToMobile :: Int,
  fPosition :: Int
  }
  deriving Show
instance FromJSON Folder where
  parseJSON (Object v) = Folder <$>
                          v .: "folder_id" <*>
                          v .: "title" <*>
                          v .: "display_title" <*>
                          v .: "sync_to_mobile" <*>
                          v .: "position"
  parseJSON _   = mzero
-- TODO: add assert that "type" == "folder"

data Highlight = Highlight {
  hlHighlightId :: HighlightId,
  hlBookmarkId :: BookmarkId,
  hlText :: Text,
  hlPosition :: Int,
  hlTime :: Integer
  }
  deriving Show
instance FromJSON Highlight where
  parseJSON (Object v) = Highlight <$>
                          v .: "highlight_id" <*>
                          v .: "bookmark_id" <*>
                          v .: "text" <*>
                          v .: "position" <*>
                          v .: "time"
  parseJSON _   = mzero
-- TODO: add assert that "type" == "highlight"

data User = User {
  uUserId :: UserId,
  uUsername :: Text
  }
  deriving Show
instance FromJSON User where
  parseJSON (Object v) = User <$>
                          v .: "user_id" <*>
                          v .: "username"
  parseJSON _   = mzero
-- TODO: add assert that "type" == "user"

data ListBookmarksResult = ListBookmarksResult {
  lbUser :: User,
  lbBookmarks :: [Bookmark],
  lbHighlights :: [Highlight],
  lbDeleteIds :: Maybe [BookmarkId]
  } deriving Show
instance FromJSON ListBookmarksResult where
  parseJSON (Object v) = ListBookmarksResult <$>
                          v .: "user" <*>
                          v .: "bookmarks" <*>
                          v .: "highlights" <*>
                          v .:? "delete_ids"
  parseJSON _   = mzero

type APIParams = [(ByteString, Maybe ByteString)]

data APIRequest apiName responseType =
  APIRequestGet {_url :: String, _params :: APIParams}
  | APIRequestPost {_url :: String, _params :: APIParams}

data InstapaperError = FromJSONError String
  deriving (Show, Typeable)
instance Exception InstapaperError
